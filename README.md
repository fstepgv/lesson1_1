# Docker Compose example

## Description

Customer provided Django blog app.

The task was to write a Docker Compose file with 3x services: 

- Nginx
- Postgresql
- Django app and all dependencies.


The solution was to use Nginx as proxy for Django blog.  User connects to Nginx , Nginx acts as proxy to Django and Django connects to postgresql.
Access to Django was done via Gunicorn.



В докер образе python:latest  (он же python:3.11)    что-то сломали, и  теперь psycorg для Джанго не собирается.    Если лень разбираться, то python:3.10  все работает как и было  (раньше он юзался как latest)
